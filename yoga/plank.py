import time

from utils import (
    calculate_angle,
    render_detections,
    get_labels_and_coordinates,
    get_angle,
    draw_ellipse,
    convert_arc,
    render_angle,
    render_angle_good,
    render_angle_bad,
    render_counter,
    render_plank_timer
)


class YogaPlank:
    def __init__(self):
        self.stage = 'WRONG'
        self.timer = None
        self.duration = 0
        self.last_duration = 0

    def exercise(self, image, keypoints):
        try:
            # left
            if all(x in keypoints for x in [11, 23, 27]):
                start, mid, end = 11, 23, 27
                # render_angle(image, keypoints, start, mid, end)

                angle = get_angle(
                    (keypoints[start], keypoints[mid]),
                    (keypoints[mid], keypoints[end])
                )

                if angle > 175:
                    render_angle_good(image, keypoints, start, mid, end)
                    if not self.timer:
                        self.timer = time.time()
                    self.duration = time.time() - self.timer
                    self.last_duration = self.duration

                    render_plank_timer(image, (100, 100),
                                       str(round(self.duration, 2)))

                else:
                    render_angle_bad(image, keypoints, start, mid, end)
                    self.timer = None
                    self.duration = 0
                    render_plank_timer(image, (100, 100),
                                       str(round(self.last_duration, 2)))





        except:
            pass


