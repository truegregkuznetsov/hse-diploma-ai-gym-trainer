import time

from utils import (
    calculate_angle,
    render_detections,
    get_labels_and_coordinates,
    get_angle,
    draw_ellipse,
    convert_arc,
    render_angle,
    render_angle_good,
    render_angle_bad,
    render_counter,
    render_plank_timer
)


class Tree:
    def __init__(self):
        self.stage = 'WRONG'
        self.timer = None
        self.duration = 0
        self.last_duration = 0

    def exercise(self, image, keypoints):
        try:
            right_arm_angle = None
            left_arm_angle = None
            knee_angle = None
            # left
            if all(x in keypoints for x in [23, 25, 27]):
                start, mid, end = 23, 25, 27
                # render_angle(image, keypoints, start, mid, end)

                knee_angle = get_angle(
                    (keypoints[start], keypoints[mid]),
                    (keypoints[mid], keypoints[end])
                )

                if knee_angle < 45:
                    render_angle_good(image, keypoints, start, mid, end)
                else:
                    render_angle_bad(image, keypoints, start, mid, end)

                render_plank_timer(image, (100, 100),
                                   str(round(self.duration, 2)))



            if all(x in keypoints for x in [23, 11, 13]):
                start, mid, end = 23, 11, 13
                # render_angle(image, keypoints, start, mid, end)

                left_arm_angle = get_angle(
                    (keypoints[start], keypoints[mid]),
                    (keypoints[mid], keypoints[end])
                )

                if left_arm_angle > 170:
                    render_angle_good(image, keypoints, start, mid, end)
                else:
                    render_angle_bad(image, keypoints, start, mid, end)

                render_plank_timer(image, (100, 100),
                                   str(round(self.duration, 2)))



            if all(x in keypoints for x in [24, 12, 14]):
                start, mid, end = 24, 12, 14
                # render_angle(image, keypoints, start, mid, end)

                right_arm_angle = get_angle(
                    (keypoints[start], keypoints[mid]),
                    (keypoints[mid], keypoints[end])
                )

                if right_arm_angle > 170:
                    render_angle_good(image, keypoints, start, mid, end)
                else:
                    render_angle_bad(image, keypoints, start, mid, end)

                render_plank_timer(image, (100, 100),
                                   str(round(self.duration, 2)))

            if left_arm_angle and right_arm_angle and knee_angle:
                if left_arm_angle > 170 and right_arm_angle > 170 and knee_angle < 45:
                    if not self.timer:
                        self.timer = time.time()
                    self.duration = time.time() - self.timer
                    self.last_duration = self.duration

                    render_plank_timer(image, (100, 100),
                                       str(round(self.duration, 2)))
                else:
                    self.timer = None
                    self.duration = 0
                    render_plank_timer(image, (100, 100),
                                       str(round(self.last_duration, 2)))





        except:
            pass


