import time

from utils import (
    calculate_angle,
    render_detections,
    get_labels_and_coordinates,
    get_angle,
    draw_ellipse,
    convert_arc,
    render_angle,
    render_angle_good,
    render_angle_bad,
    render_counter,
    render_plank_timer
)


class Goddes:
    def __init__(self):
        self.stage = 'WRONG'
        self.timer = None
        self.duration = 0
        self.last_duration = 0

    def exercise(self, image, keypoints):
        try:
            # right_arm_angle = None
            # left_arm_angle = None
            left_knee_angle = None
            right_knee_angle = None
            # left knee
            if all(x in keypoints for x in [23, 25, 27]):
                start, mid, end = 23, 25, 27
                # render_angle(image, keypoints, start, mid, end)

                left_knee_angle = get_angle(
                    (keypoints[start], keypoints[mid]),
                    (keypoints[mid], keypoints[end])
                )

                if 80 < left_knee_angle < 100:
                    render_angle_good(image, keypoints, start, mid, end)
                else:
                    render_angle_bad(image, keypoints, start, mid, end)

                render_plank_timer(image, (100, 100),
                                   str(round(self.duration, 2)))

            # right knee
            if all(x in keypoints for x in [24, 26, 28]):
                start, mid, end = 24, 26, 28
                # render_angle(image, keypoints, start, mid, end)

                right_knee_angle = get_angle(
                    (keypoints[start], keypoints[mid]),
                    (keypoints[mid], keypoints[end])
                )

                if 80 < right_knee_angle < 100:
                    render_angle_good(image, keypoints, start, mid, end)
                else:
                    render_angle_bad(image, keypoints, start, mid, end)

                render_plank_timer(image, (100, 100),
                                   str(round(self.duration, 2)))


            if left_knee_angle and right_knee_angle:
                if 80 < left_knee_angle < 100 and 80 < right_knee_angle < 100:
                    if not self.timer:
                        self.timer = time.time()
                    self.duration = time.time() - self.timer
                    self.last_duration = self.duration

                    render_plank_timer(image, (100, 100),
                                       str(round(self.duration, 2)))
                else:
                    self.timer = None
                    self.duration = 0
                    render_plank_timer(image, (100, 100),
                                       str(round(self.last_duration, 2)))





        except:
            pass


