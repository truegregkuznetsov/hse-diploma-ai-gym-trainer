from abc import ABC

import pandas as pd
import timm
from PIL import Image
from torchvision import transforms as T
import torch
import joblib

class GymClassifier(ABC):
    def __init__(self):
        self.model = joblib.load('mp-rf-gym.pkl')
        self.id2label = {0: 'squat', 1: 'plank', 2: 'hammer curl'}

    def predict(self, landmarks):
        points = []
        columns = []
        try:
            for idd, landmark in enumerate(landmarks.landmark):
                x = landmark.x
                y = landmark.y

                points.append(x)
                points.append(y)
                columns.append(f'X {idd}')
                columns.append(f'Y {idd}')

            sample = pd.DataFrame(points).T
            sample.columns = columns
            # print(sample)
            pred = self.model.predict(sample)
            return self.id2label[pred[0]]
        except Exception as e:
            pass




if __name__ == '__main__':
    gc = GymClassifier()

    print(gc)



