import cv2
import mediapipe as mp
import numpy as np

from utils import (calculate_angle,
                   render_detections,
                   get_labels_and_coordinates,
                   get_angle,
                   draw_ellipse,
                   convert_arc,
                   render_angle,
                   render_yoga_pose)

from gym.bicep_curl import BicepCurl
from gym.plank import Plank
from gym.squat import Squat

from yoga.downdog import Downdog
from yoga.tree import Tree
from yoga.warrior import Warrior
from yoga.plank import YogaPlank
from yoga.goddes import Goddes

from model.yoga_classifier import YogaClassifier
from model.gym_classifier import GymClassifier



mp_drawing = mp.solutions.drawing_utils
mp_pose = mp.solutions.pose
mp_holistic = mp.solutions.holistic

pose = mp_pose.Pose()


cap = cv2.VideoCapture(0)

bicep_curl = BicepCurl()
plank = Plank()
squat = Squat()

downdog = Downdog()
tree = Tree()
warrior = Warrior()
yoga_plank = YogaPlank()
goddes = Goddes()


yc = YogaClassifier()
gc = GymClassifier()

with pose:
    while cap.isOpened():
        ret, frame = cap.read()
        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image.flags.writeable = False
        results = pose.process(image)
        landmarks = results.pose_landmarks
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        keypoints = get_labels_and_coordinates(image, results)
        goddes.exercise(image, keypoints)

        pose_class = yc.predict(landmarks)

        render_yoga_pose(image, (100, 200), pose_class)

        # render detections
        render_detections(image, results)
        cv2.imshow('Mediapipe Feed', image)
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
    for i in range(2):
        cv2.waitKey(1)
