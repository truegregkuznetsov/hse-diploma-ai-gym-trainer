
from utils import (calculate_angle,
                   render_detections,
                   get_labels_and_coordinates,
                   get_angle,
                   draw_ellipse,
                   convert_arc,
                   render_angle,
                   render_counter,
                   render_angle_good,
                   render_angle_bad)


class BicepCurl:
    def __init__(self):
        self.left_counter = 0
        self.right_counter = 0
        self.counter = 0
        self.left_stage = 'DOWN'
        self.right_stage = 'DOWN'

    def exercise(self, image, keypoints):
        try:
            # left

            if all(x in keypoints for x in [23, 11, 13]):
                start, mid, end = 23, 11, 13
                # render_angle(image, keypoints, start, mid, end)

                left_shoulder_angle = get_angle(
                    (keypoints[start], keypoints[mid]),
                    (keypoints[mid], keypoints[end])
                )

                if left_shoulder_angle < 30:
                    render_angle_good(image, keypoints, start, mid, end)
                else:
                    render_angle_bad(image, keypoints, start, mid, end)

            if all(x in keypoints for x in [24, 12, 14]):
                start, mid, end = 24, 12, 14
                # render_angle(image, keypoints, start, mid, end)

                right_shoulder_angle = get_angle(
                    (keypoints[start], keypoints[mid]),
                    (keypoints[mid], keypoints[end])
                )

                if right_shoulder_angle < 30:
                    render_angle_good(image, keypoints, start, mid, end)
                else:
                    render_angle_bad(image, keypoints, start, mid, end)

            if all(x in keypoints for x in [12, 14, 16]):
                start, mid, end = 12, 14, 16
                # render_angle(image, keypoints, start, mid, end)

                left_angle = get_angle(
                    (keypoints[start], keypoints[mid]),
                    (keypoints[mid], keypoints[end])
                )

                if self.left_stage == 'UP' and left_angle > 90:
                    self.left_stage = 'DOWN'

                if self.left_stage == 'DOWN' and left_angle < 90:
                    self.left_stage = 'UP'
                    self.left_counter += 1
                    self.counter += 1

                render_counter(
                    image,
                    keypoints[12],
                    self.left_counter,
                    self.left_stage
                )

            # right
            if all(x in keypoints for x in [11, 13, 15]):
                start, mid, end = 11, 13, 15
                # render_angle(image, keypoints, start, mid, end)

                right_angle = get_angle(
                    (keypoints[start], keypoints[mid]),
                    (keypoints[mid], keypoints[end])
                )

                if self.right_stage == 'UP' and right_angle > 90:
                    self.right_stage = 'DOWN'

                if self.right_stage == 'DOWN' and right_angle < 90:
                    self.right_stage = 'UP'
                    self.right_counter += 1
                    self.counter += 1

                render_counter(
                    image,
                    keypoints[11],
                    self.right_counter,
                    self.right_stage
                )

            render_counter(
                image,
                (50, 50),
                self.counter,
                'OVERALL'
            )

        except:
            pass


