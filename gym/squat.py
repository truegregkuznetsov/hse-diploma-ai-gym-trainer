from utils import (
    calculate_angle,
    render_detections,
    get_labels_and_coordinates,
    get_angle,
    draw_ellipse,
    convert_arc,
    render_angle,
    render_angle_good,
    render_angle_bad,
    render_counter,
    render_plank_timer
)


class Squat:
    def __init__(self):
        self.counter = 0
        self.left_stage = 'DOWN'
        self.right_stage = 'DOWN'
        self.left_counter = False
        self.right_counter = False

    def exercise(self, image, keypoints):
        try:
            self.left_counter = False
            self.right_counter = False

            # back
            if all(x in keypoints for x in [12, 24, 26]):
                start, mid, end = 12, 24, 26
                # render_angle(image, keypoints, start, mid, end)

                back_angle = get_angle(
                    (keypoints[start], keypoints[mid]),
                    (keypoints[mid], keypoints[end])
                )

                if back_angle > 45:
                    render_angle_good(image, keypoints, start, mid, end)
                else:
                    render_angle_bad(image, keypoints, start, mid, end)


            # left knee
            if all(x in keypoints for x in [24, 26, 28]):
                start, mid, end = 24, 26, 28
                # render_angle(image, keypoints, start, mid, end)

                left_knee_angle = get_angle(
                    (keypoints[start], keypoints[mid]),
                    (keypoints[mid], keypoints[end])
                )

                if self.left_stage == 'UP' and left_knee_angle >= 90:
                    self.left_stage = 'DOWN'

                elif self.left_stage == 'DOWN' and left_knee_angle < 90:
                    self.left_stage = 'UP'
                    self.left_counter = True
                    self.counter += 1

            # right knee
            if all(x in keypoints for x in [23, 25, 27]):
                start, mid, end = 23, 25, 27
                # render_angle(image, keypoints, start, mid, end)

                right_knee_angle = get_angle(
                    (keypoints[start], keypoints[mid]),
                    (keypoints[mid], keypoints[end])
                )

                if self.right_stage == 'UP' and right_knee_angle >= 90:
                    self.right_stage = 'DOWN'

                elif self.right_stage == 'DOWN' and right_knee_angle < 90:
                    self.right_stage = 'UP'
                    self.right_counter = True
                    # self.counter += 1


            render_counter(image, (100, 100), self.counter, '')











        except:
            pass


